import React from 'react';
import fire from '../src/fire';

class Email extends React.Component {

  constructor(props) {
    super(props);
    this.addEmail = this.addEmail.bind(this);

    this.state = {
      submitted: false
    }
  }

  addEmail(e) {
    e.preventDefault();
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailEntry = this.inputEl.value
    const storedEmails = this.props.emails;
    let unique = true;
    storedEmails.forEach(email => {
      if (email.text == emailEntry) unique = false;
    })
    if (re.test(emailEntry) && unique) {
      fire.database().ref('emails').push(emailEntry);
      this.inputEl.value = '';
      this.setState({ submitted: true });
    } else if (!unique) {
      this.inputEl.value = ''
      alert('Duplicate Email');
    } else {
      this.inputEl.value = ''
      alert('Input Incorrect');
    }
  }

  render() {
    return (
      <div>
      <div className="submission">
        <form className="email" onSubmit={(e) => this.addEmail(e)}>
          <input className="email-text" name="email" type="text" ref={ el => this.inputEl = el } placeholder="Enter Your Email"></input>
          <input className="email-submit" type="submit"></input>
        </form>
      </div>
      <div className="footer">
            {
              this.state.submitted ? <div className="submitted">Thank You For Your Submission</div> : <div className="waiting">Please enter your email above to receive the latest and greatest cashback offers. You are one click away from becoming a professional shopper!</div>
            }
      </div>
      </div>
    )
  }
}
export default Email;
